function FormSection ( ) {
  
  var instance, jQueries, internal;
  instance = this;
  internal = {};
	jQueries = {};
  
  instance.setElementJQuery = function setElementJQuery ( _jQueryInstance ) {
    jQueries.element = jQuery('.form-section');
    
    jQueries.centerMarginWrap = jQueries.element.find ( '.form-section-center-margin-wrap' );
    jQueries.mainCopy = jQueries.centerMarginWrap.children ( '.main-copy' );
    jQueries.mainCopyItems = jQueries.mainCopy.children (  );
    jQueries.mainCopyItemsItems = jQueries.mainCopyItems.children (  );
    jQueries.formSnippet = jQueries.centerMarginWrap.children ( '.form-snippet' );
    jQueries.requiredNote = jQueries.formSnippet.children ( '.form-section-required-note' );
    jQueries.fieldList = jQueries.formSnippet.children ( '.field-list' );
    jQueries.halfWidth = jQueries.fieldList.children ( '.half-width' );
    jQueries.fieldLabel = jQueries.halfWidth.children ( '.field-label' );
    jQueries.textField = jQueries.halfWidth.children ( '.text-field' );
    jQueries.fullWidth = jQueries.fieldList.children ( '.full-width' );
    jQueries.fieldLabel = jQueries.fieldLabel.add ( jQueries.fullWidth.children ( '.field-label' ) );
    jQueries.textField = jQueries.textField.add ( jQueries.fullWidth.children ( '.text-field' ) );
    jQueries.fieldListItems = jQueries.fieldList.children ( );
    jQueries.fieldLabel = jQueries.fieldLabel.add ( jQueries.fieldListItems.children ( '.field-label' ) );
    jQueries.formCheckInputList = jQueries.fieldListItems.children ( '.form-check-input-list' );
    jQueries.formCheckInputListItems = jQueries.formCheckInputList.children (  );
    jQueries.formCheckInput = jQueries.formCheckInputListItems.children ( '.form-check-input' );
    jQueries.formCheckLabel = jQueries.formCheckInputListItems.children ( '.form-check-label' );
    jQueries.smallFieldLabel = jQueries.fieldLabel.children ( '.small-field-label' );
    jQueries.messageField = jQueries.fieldListItems.children ( '.message-field' );
    jQueries.formActionList = jQueries.formSnippet.children ( '.form-action-list' );
    jQueries.formActionListItems = jQueries.formActionList.children (  );
    jQueries.formActionState = jQueries.formActionListItems.children ( '.form-action-state' );
    jQueries.formActionButton = jQueries.formActionState.children ( '.form-action-button' );
    jQueries.errorMessage = jQueries.centerMarginWrap.find('.error-message');
    jQueries.successMessage = jQueries.centerMarginWrap.find('.success-message');
    jQueries.formErrorWrapperMessage = jQueries.centerMarginWrap.find('.form-action-validation-message');
  };
  
  instance.init = function ( ) {
    validateForm();
    jQueries.textField.focus(addIsActive);
    jQueries.textField.focusout(removeIsActive);
    jQueries.messageField.focus(addIsActive);
    jQueries.messageField.focusout(removeIsActive);
    jQueries.textField.on('input propertychange paste', checkAutofill);
  } 
  
function showErrorMessage(){
  jQueries.formSnippet.fadeIn("slow").addClass("fade");
  jQueries.errorMessage.fadeIn("slow").addClass("visible");
} 
  
function showSuccessMessage(){
  jQueries.formSnippet.fadeIn("slow").addClass("fade");
  jQueries.successMessage.fadeIn("slow").addClass("visible");
}
  
function validateForm(){
  
  jQueries.formSnippet.validate({
    errorPlacement: function(error, element) {
      if ($(element).is(':checkbox')) {
            $(element).parent().parent().siblings().addClass('error');
        } else {
            $(element).addClass('error');
        }
    },
    highlight: function(element) {
        if ($(element).is(':checkbox')) {
            $(element).parent().parent().siblings().addClass('error');
        } else {
            $(element).addClass('error');
        }
    },
    unhighlight: function(element) {
        if ($(element).is(':checkbox')) {
            $(element).parent().parent().siblings().removeClass('error');
        } else {
            $(element).removeClass('error');
        }
    },
    rules: {
      firstname: {
          required: true
      },
      lastname: {
          required: true
      },
      email: {
          required: true,
          email: true
      },
      message: {
          required: true
      },
      'type[]': {
        required: true,
        minlength: 1
      }
    },
    messages: {
      firstname: {
          required: ""
      },
      lastname: {
          required: ""
      },
      email: {
          required: "",
          email: ""
      },
      message: {
          required: ""
      },
      'type[]': {
        required: "",
        minlength: ""
      }
    },
    invalidHandler: function(form, validator) {
      var errors = validator.numberOfInvalids();
      if (errors) {
        jQueries.formErrorWrapperMessage.addClass('visible');
        //jQueries.formActionButton.addClass('disabled');
      }
    },
    
    submitHandler: function(form) {
      jQueries.formErrorWrapperMessage.removeClass('visible'); 
      $(form).ajaxSubmit({
        type:"POST",
        data: $(form).serialize(),
        url:"process.php",
        success: showSuccessMessage,
        error: showErrorMessage
      });
    }
  });
}

function addIsActive ( ) {
    jQuery(this).parent().addClass("is-active is-completed");
  }
  
  function removeIsActive(){
    if (jQuery(this).val() === ""){
      jQuery(this).parent().removeClass("is-completed");
      jQuery(this).parent().removeClass("is-active");
    }
  }
  
  function checkAutofill(){
    jQuery(this).parent().addClass("is-active is-completed");
  }
  
}