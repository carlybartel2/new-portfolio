/*@codekit-append "../sections/form/form.js"*/

var site;

function Site () {

	var instance, jQueries, internal;
	instance = this;
	internal = {};
	jQueries = {};

	function onReady ( _event ) {
	  window.isReady = true;
	  createFormSection();
	  createCopyrightDate();
	  changeColor();
	  showCurrentSong();
	  createRandomQuotes();
	  scrollToTarget();
	  getWeather();
	  filterWork();
	  height = $(".quote-section").outerHeight(true);
	  $nav.css("top", height);
    $work.css("margin-top", height);
	}
	
	function createFormSection ( _index, _element ) {
	  var _module;
	  _module = new FormSection();
	  _module.setElementJQuery ( _element );
	  _module.init();
  }
  
  function createCopyrightDate() {
    var date = new Date();
    var year = date.getFullYear();
    document.getElementById("copyright-text").innerHTML = 'Copyright ' + year +  '.';
  }
  
  function changeColor(){
    var classes = ['pink', 'blue', 'teal'];
    var randomnumber = Math.floor(Math.random()*classes.length);
    jQuery('body').addClass(classes[randomnumber]); 
  }
  
  function showCurrentSong(){
    jQuery('#lastFmWidget').lastfmNowPlaying({
    	apiKey: 'acd6a525709730f29a5cd6d294193722',
    	members: ['carlybartel']
    });
  }
  
  function createRandomQuotes(){
    var myQuotes = new Array();
    
    myQuotes[0] = html = '<span class="quote-text">You are a mashup of what you let into your life.</span><span class="quote-author">Austin Kleon</span>';
    myQuotes[1] = html = '<span class="quote-text">I&rsquo;ve learned that I still have a lot to learn.</span><span class="quote-author">Maya Angelou</span>';
    myQuotes[2] = html = '<span class="quote-text">Practicing an art, no matter how well or badly, is a way to make your soul grow.</span><span class="quote-author">Kurt Vonnegut</span>';
    
    var myRandom = Math.floor(Math.random()*myQuotes.length);
    
    jQuery('.quote-text-wrap').html(myQuotes[myRandom]);
  }
  
  function scrollToTarget(){
    jQuery('a[href^="#"]').on('click',function (e) {
  	    e.preventDefault();
  	    var target = this.hash;
  	    var $target = $(target);
  
  	    jQuery('html, body').stop().animate({
  	        'scrollTop': $target.offset().top
  	    }, 900, 'swing', function () {
  	        window.location.hash = target;
  	    });
  	});
  } 
  
  function getWeather(){
    $.simpleWeather({
      location: 'Cleveland, OH',
      woeid: '',
      unit: 'f',
      success: function(weather) {
        html = '<span class="temperature"><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+' and </span>';
        html += '<div class="weather-description">'+weather.currently+' here in ';
        html += weather.city+', '+weather.region+'</div>';
        if(weather.temp > 70){
          html += '<div class="weather-commentary">Definitely outside gettin some freckles.';
        }
        if(weather.temp > 50 && weather.temp < 70){
          html += '<div class="weather-commentary">Could be a ~little~ warmer.';
        }
        if(weather.temp > 32 && weather.temp < 50){
          html += '<div class="weather-commentary">so im probably pretty cold.';
        }
        if(weather.temp < 32){
          html += '<div class="weather-commentary">Definitely under ~1000 blankets.';
        }
    
        $("#weather").html(html);
      },
      error: function(error) {
        $("#weather").html('<p>'+error+'</p>');
      }
    });
  }
  
  jQuery('.gallery-item').featherlightGallery({
  	previousIcon: '',
        nextIcon: ''
	});
	
	function filterWork (){
  	$('.category').on('change', function(){
      var category_list = [];
      $('.filter-list :input:checked').each(function(){
        var category = $(this).val();
        category_list.push(category);
      });
  
      if(category_list.length == 0)
        $('.work-list li').fadeIn();
      else {
        $('.work-list li').each(function(){
          var item = $(this).attr('data-tag');
          var itemarr = item.split(',');
          $this = $(this);
          $.each(itemarr,function(ind,val){
            if(jQuery.inArray(val,category_list) > -1){
              $this.fadeIn('slow');
              $this.addClass('visible');
              return false;
            }
            else
              $this.hide();
              $this.removeClass('visible');
          });
        });
      }   
    });
	}
  
  var $document = $(document),
    $element = $('.overflow-wrap'),
    $nav = $('.home-nav'),
    $work = $('.work-section'),
    className = 'fixed';
    height = $(".quote-section").outerHeight(true);
    
  $document.scroll(function() {
    if ($document.scrollTop() >= height) {
      $element.addClass(className);
      $nav.css("top", 0);
      $('#work').addClass('current');
    } else {
      $element.removeClass(className);
      $nav.css("top", height);
      $work.css("margin-top", height);
      $('#work').removeClass('current');
    }
  });
	
	jQuery( onReady );

}

site = new Site();